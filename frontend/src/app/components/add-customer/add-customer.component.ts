import { Component, OnInit, NgZone } from '@angular/core';
import { CustomerService } from '../../../api/services/CustomerService';
import {FormControl, FormBuilder, FormGroup, FormGroupDirective, NgForm, Validators} from '@angular/forms';
import { Router } from '@angular/router';

import {ErrorStateMatcher} from '@angular/material/core';

/** Error when invalid control is dirty, touched, or submitted. */
export class MyErrorStateMatcher implements ErrorStateMatcher {
  isErrorState(control: FormControl | null, form: FormGroupDirective | NgForm | null): boolean {
    const isSubmitted = form && form.submitted;
    return !!(control && control.invalid && (control.dirty || control.touched || isSubmitted));
  }
}

@Component({
  selector: 'app-add-customer',
  templateUrl: './add-customer.component.html',
  styleUrls: ['./add-customer.component.scss']
})
export class AddCustomerComponent implements OnInit {
  customerForm!: FormGroup;
  CustomerArr: any = [];
  fullnameFormControl = new FormControl('', [Validators.required]);
  emailFormControl = new FormControl('', [Validators.required, Validators.email]);
  phoneFormControl = new FormControl('', [Validators.required, Validators.pattern("^[0-9]{2}\-[0-9]{8,9}$")]);
  streetFormControl = new FormControl('', [Validators.required]);
  numberFormControl = new FormControl('', [Validators.required, Validators.pattern("^[0-9]{1,100}")]);
  districtFormControl = new FormControl('', [Validators.required]);
  cityFormControl = new FormControl('', [Validators.required]);
  stateFormControl = new FormControl('', [Validators.required]);
  zipcodeFormControl = new FormControl('', [Validators.required, Validators.pattern("^[0-9]{5}\-[0-9]{3}")]);
  occupationFormControl = new FormControl('', [Validators.required]);

  matcher = new MyErrorStateMatcher();
  ngOnInit() {
    this.addCustomer();
  }
  constructor(
    public fb: FormBuilder,
    private ngZone: NgZone,
    private router: Router,
    public customerService: CustomerService
  ) {}
  addCustomer() {
    this.customerForm = this.fb.group({
      fullname: [''],
      email: [''],
      phone: [''],
      address_street: [''],
      address_number: [''],
      address_district: [''],
      address_city: [''],
      address_state: [''],
      address_zipcode: [''],
      address_complement: [''],
      occupation: [''],
      resume: ['']
    });
  }

  submitForm() {
    this.customerService.routeCreateCustomerCustomerPost(this.customerForm.value).subscribe((res) => {
      alert("Customer successfully added!")
      this.ngZone.run(() => this.router.navigateByUrl('customers-list'));
    });
  }

}
