from typing import Optional
from pydantic import BaseModel, EmailStr, Field

class CustomerSchema(BaseModel):
    fullname: Optional[str] = Field(
        title="Customer's full name.", max_length=300
    )
    email: EmailStr = Field(...)
    phone: Optional[str] = Field(
        title="Phone number",
        description="Phone in brazilian local format: 51-999999999 with a optional '9 digit.'",
        regex="^[0-9]{2}\-[0-9]{8,9}$"
    )
    address_street: Optional[str] = Field(
        title="Address street"
    )
    address_number: Optional[str] = Field(
        title="House number"
    )
    address_district: Optional[str] = Field(
        title="District"
    )
    address_city: Optional[str] = Field(
        title="City"
    )
    address_state: Optional[str] = Field(
        title="State"
    )
    address_zipcode: Optional[str] = Field(
        title="Zipcode",
        description="Zipcode in brazilian format: 95555-000"
    )
    address_complement: Optional[str] = Field(
        title="Address complement"
    )
    occupation: Optional[str] = Field(
        title="Occupation",
        description="Customer's job occupation"
    )
    resume: Optional[str] = Field(
        title="Resume",
        description="Customer's resume file URL"
    )

    class Config:
        schema_extra = {
            "example": {
                "fullname": "João da Silva Sauro",
                "email": "joao.silva@email.com",
                "phone": "54-998452136",
                "address_street": "Rua dos Bobos",
                "address_number": "0",
                "address_district": "Centro",
                "address_city": "Nova Petrópolis",
                "address_state": "RS",
                "address_zipcode": "95150-000",
                "address_complement": "Casa Engraçada",
                "occupation": "Desenvolvedor Cobol",
                "resume": "https://google.com"
            }
        }

class UpdateCustomerModel(BaseModel):
    fullname: Optional[str] = Field(
        title="Customer's full name.", max_length=300
    )
    email: Optional[EmailStr]
    phone: Optional[str] = Field(
        title="Phone number",
        description="Phone in brazilian local format: 51-999999999 with a optional '9 digit.'",
        regex="^[0-9]{2}\-[0-9]{8,9}$"
    )
    address_street: Optional[str] = Field(
        title="Address street"
    )
    address_number: Optional[str] = Field(
        title="House number"
    )
    address_district: Optional[str] = Field(
        title="District"
    )
    address_city: Optional[str] = Field(
        title="City"
    )
    address_state: Optional[str] = Field(
        title="State"
    )
    address_zipcode: Optional[str] = Field(
        title="Zipcode",
        description="Zipcode in brazilian format: 95555-000",
        regex="^[0-9]{5}\-[0-9]{3}$"
    )
    address_complement: Optional[str] = Field(
        title="Address complement"
    )
    occupation: Optional[str] = Field(
        title="Occupation",
        description="Customer's job occupation"
    )
    resume: Optional[str] = Field(
        title="Resume",
        description="Customer's resume file URL"
    )

    class Config:
        schema_extra = {
            "example": {
                "fullname": "João da Silva Sauro",
                "email": "joao.silva@email.com",
                "phone": "54-998452136",
                "address_street": "Rua dos Bobos",
                "address_number": "0",
                "address_district": "Centro",
                "address_city": "Nova Petrópolis",
                "address_state": "RS",
                "address_zipcode": "95150-000",
                "address_complement": "Casa Engraçada",
                "occupation": "Desenvolvedor Cobol",
                "resume": "https://google.com"
            }
        }

def ResponseModel(data, message):
    return {
        "data": [data],
        "code": 200,
        "message": message,
    }

def ErrorResponseModel(error, code, message):
    return {"error": error, "code": code, "message": message}