from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware

from routes.customer import router as CustomerRouter

api = FastAPI(
    title="FastAPI",
    description="API",
    version="1.0.0",
    contact={
        "name": "Dennis Anderson de Souza",
        "url": "https://osouza.de",
        "email": "gmail@osouza.de",
    },
    license_info={
        "name": "Apache 2.0",
        "url": "https://www.apache.org/licenses/LICENSE-2.0.html",
    },
    root_path = "/api",
    # openapi_url='/api/openapi.json'
)
api.add_middleware(
    CORSMiddleware,
    allow_origins=["*"],
    allow_credentials=False,
    allow_methods=["*"],
    allow_headers=["*"],
)

api.include_router(CustomerRouter, tags=["Customer"], prefix="/customer")

@api.get("/")
async def read_root():
    return {"message": "Hello, world."}
