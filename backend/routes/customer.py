from fastapi import APIRouter, Body
from fastapi.encoders import jsonable_encoder

from database import (
    list_customers,
    create_customer,
    read_customer,
    update_customer,
    delete_customer,
)
from models.customer import (
    ErrorResponseModel,
    ResponseModel,
    CustomerSchema,
    UpdateCustomerModel,
)

router = APIRouter()

@router.get("/", response_description="Retrieve a list of every customer")
async def route_list_customers():
    customers = await list_customers()
    if customers:
        return ResponseModel(customers, "Customers data retrieved successfully")
    return ResponseModel(customers, "Empty list returned")

@router.post("/", response_description="Insert a new customer data into the system")
async def route_create_customer(customer: CustomerSchema = Body(...)):
    customer = jsonable_encoder(customer)
    new_customer = await create_customer(customer)
    return ResponseModel(new_customer, "Customer added successfully.")

@router.get("/{id}", response_description="Retrieve a customer's data")
async def route_read_customer(id):
    customer = await read_customer(id)
    if customer:
        return ResponseModel(customer, "Customer data retrieved successfully")
    return ErrorResponseModel("An error occurred.", 404, "Customer doesn't exist.")

@router.put("/{id}")
async def route_update_customer(id: str, req: UpdateCustomerModel = Body(...)):
    req = {k: v for k, v in req.dict().items() if v is not None}
    updated_customer = await update_customer(id, req)
    if updated_customer:
        return ResponseModel(
            "Customer with ID: {} name update is successful".format(id),
            "Customer name updated successfully",
        )
    return ErrorResponseModel(
        "An error occurred",
        404,
        "There was an error updating the customer data.",
    )

@router.delete("/{id}", response_description="Delete a customer data from the system")
async def route_delete_customer(id: str):
    deleted_customer = await delete_customer(id)
    if deleted_customer:
        return ResponseModel(
            "Customer with ID: {} removed".format(id), "Customer deleted successfully"
        )
    return ErrorResponseModel(
        "An error occurred", 404, "Customer with id {0} doesn't exist".format(id)
    )
